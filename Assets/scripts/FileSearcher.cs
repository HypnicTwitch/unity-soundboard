using UnityEngine;
using System.IO;

[CreateAssetMenu(fileName="New File Searcher", menuName="Cat Assets/File Searcher")]

public class FileSearcher : ScriptableObject {

    private string _audioFileType = "wav";

    public string AudioFileType { get { return _audioFileType; } set { _audioFileType = value; } }

    /***
     */

    public void /*AudioFile[]*/ GetAudioFiles(string audioFilePath, string myAudioFileType)
    {
        FileInfo[] info;
        //this directory is the root that the file searcher will recursively traverse to find audio files
        DirectoryInfo dir = new DirectoryInfo(audioFilePath);
        info = dir.GetFiles("*."+myAudioFileType);
        foreach(FileInfo f in info)
        {
            Debug.Log(f.ToString());
        }
        //FIXME: change this return a container of audio files
        //return null;
    }


}